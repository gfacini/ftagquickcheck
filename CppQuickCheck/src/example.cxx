#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTaggingContainer.h"
#include "TFile.h"
#include "TTree.h"

// function to check return codes, you can also check them manually
void check_rc(StatusCode code) {
  if (!code.isSuccess()) throw std::runtime_error("bad return code");
}


int main(int nargs, char* argv[]) {

  // set up accessors
  typedef ElementLink< xAOD::BTaggingContainer> btaggingLink;
  const SG::AuxElement::ConstAccessor<btaggingLink> blink("btaggingLink");
  const SG::AuxElement::ConstAccessor<float> jf_mass("JetFitter_mass");

  if (nargs <= 1) throw std::runtime_error("need one input");

  // set up file reading
  check_rc( xAOD::Init() );
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  std::cout << "looking at " << nargs - 1 << " files" << std::endl;

  // loop over input files
  for (int fn = 0; fn < nargs; fn++) {

    // open and check that the input file is ok
    std::string file(argv[fn+1]);
    std::cout << "opening " << file << std::endl;
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      throw std::runtime_error("couldn't open file");
    }
    // connect the input file
    check_rc( event.readFrom(ifile.get()) );

    // main event loop
    for (long long int evtn = 0; evtn < event.getEntries(); evtn++) {

      event.getEntry(evtn);
      std::cout << "loaded event " << evtn << std::endl;

      // read in a jet collection. You should change this, it's just
      // what I had on hand.
      const xAOD::JetContainer* jets = nullptr;
      check_rc(event.retrieve(jets, "AntiKt4EMTopoJets"));

      // loop over jets
      for (const xAOD::Jet* jet : *jets) {

        // print some jet info
        std::cout << "jet pt" << jet->pt() << std::endl;

        // access the b-tagging object (note all the `*`s)
        const xAOD::BTagging* btag = *blink(*jet);

        // print some b-tagging info
        std::cout << "jf mass: " << jf_mass(*btag) << std::endl;
      }
    }
  }

  return 0;
}
