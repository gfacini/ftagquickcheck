# A quick script to flip the IP signs in the dumped h5 files to
# aid the flip tagger studies
# author: bingxuan.liu@cern.ch
# Modes:
#     simple: flip the signs of variables included in flipVariables
#     negative_ip: change variables in flipVariables to NaN for tracks whose keyFlipVariable has a positive sign
#                  and then flip the signs of the remaining variables
#     negative_track: change all track variables (otherTrackVariables + flipVariables) to NaN for tracks whose  #                  keyFlipVariable has a positive sign and then flip the signs of the remaining flipVariables


import h5py
import os,sys
import uproot
import numpy as np
import optparse

parser = optparse.OptionParser()
parser.add_option('-i','--inputFile',   dest='inputFile', help='Specify the path to the input file.', default='')
parser.add_option('-o','--outputFile',   dest='outputFile', help='Specify the path to the output file.', default='')
parser.add_option('-m','--flipMode',   dest='flipMode', help='Define the flipping strategy.', default="simple")
parser.add_option('-k','--keyFlipVariable',   dest='keyFlipVariable', help='Define the key flipping variable. Decide whether to drop variables or tracks based on its sign.', default="IP3D_signed_d0_significance")
parser.add_option('-a','--additionalKeyFlipVariable',   dest='additionalKeyFlipVariable', help='Define an additional key flipping variable. Remove the tracks if its sign is positive.', default="")
(arguments, args) = parser.parse_args()

if not arguments.inputFile or not arguments.outputFile:
  print("No input or output given! Aborting")
  sys.exit(0)

# Key variable to decide when to drop tracks
keyFlipVariable = arguments.keyFlipVariable
additionalKeyFlipVariable = ""
if arguments.additionalKeyFlipVariable:
  additionalKeyFlipVariable = arguments.additionalKeyFlipVariable

flipVariables = [
  "IP3D_signed_d0_significance",
  "IP3D_signed_z0_significance",
  "d0",
  "z0SinTheta",
]

otherTrackVariables = [
  "numberOfInnermostPixelLayerHits",
  "numberOfNextToInnermostPixelLayerHits",
  "numberOfInnermostPixelLayerSharedHits",
  "numberOfInnermostPixelLayerSplitHits",
  "numberOfPixelSharedHits",
  "numberOfPixelSplitHits",
  "numberOfSCTSharedHits",
  "ptfrac",
  "dr",
  "numberOfPixelHits",
  "numberOfSCTHits",
  #"d0",
  #"z0SinTheta",
]

# For safety always copy the input to an output
os.system("cp " + arguments.inputFile + " " + arguments.outputFile)

inputFile = h5py.File(arguments.outputFile,'r+')

#Get the variables that decide where to flip
keyData = inputFile['tracks_loose'][keyFlipVariable]
addKeyData = 0
if additionalKeyFlipVariable != "":
  addKeyData =  inputFile['tracks_loose'][additionalKeyFlipVariable]

iteration = 0

for var in flipVariables :

  #additionalKeyFlipVariable Simple flip: only change the signs for all variables in flipVariables
  if arguments.flipMode == "simple":
    data = inputFile['tracks_loose'][var]
    inputFile['tracks_loose'][var] = -1 * data[...]

  # Negative tag: remove the positive signs according to signed_d0 nd then flip the negative signs
  if "negative" in arguments.flipMode:
    data = inputFile['tracks_loose'][var]
    # Change the value to 0 if the track has positive keyFlipVariable
    data[keyData[:,:] > 0] = 0
    if additionalKeyFlipVariable != "":
      data[addKeyData[:,:] > 0] = 0

    # Also flip the signs for those IP variables
    inputFile['tracks_loose'][var] = -1 * data[...]

    if iteration == 0:
      data = inputFile['tracks_loose']['valid']
      data[keyData[:,:] > 0] = False
      if additionalKeyFlipVariable != "":
        data[addKeyData[:,:] > 0] = False
      inputFile['tracks_loose']['valid'] = data[...]
    # Change all the other track variables to NaN too if flipMode == "negative_tracks"
    if arguments.flipMode == "negative_tracks" and iteration == 0:
      for trkVar in otherTrackVariables:
        data = inputFile['tracks_loose'][trkVar]
        data[keyData[:,:] > 0] = 0
        if additionalKeyFlipVariable != "":
          data[addKeyData[:,:] > 0] = 0
        inputFile['tracks_loose'][trkVar] = data[...]
      iteration = iteration + 1
inputFile.close()
