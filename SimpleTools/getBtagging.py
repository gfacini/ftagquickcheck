# This is a minimal setup to retrive b-tagging information in the DAOD using python
# No CP tools are scheduled but for quick checks this can be useful
# Author: Bingxuan Liu (bingxuan.liu@cern.ch)
# In this example, we try to create 2D histograms beamspot size vs b-tagger score 
# run this from an environment that has asetup AnalysisBase,blahblah

import ROOT
if(not ROOT.xAOD.Init().isSuccess()): print('Failed xAOD.Init()')
# A C macro is needed since element links are difficult to handle in PyRoot (to my knowledge)
ROOT.gROOT.ProcessLine('.L getBtagging.C')
ROOT.gROOT.SetBatch(1)

# Define the list of files to use, either locally, or via xrootd.
files = [                                         
  'root://sdrm.t1.grid.kiae.ru:1094//t1.grid.kiae.ru/data/atlas/atlasdatadisk/rucio/mc21_13p6TeV/82/49/DAOD_FTAG1.30286384._001415.pool.root.1',
  'root://sdrm.t1.grid.kiae.ru:1094//t1.grid.kiae.ru/data/atlas/atlasdatadisk/rucio/mc21_13p6TeV/10/c9/DAOD_FTAG1.30286384._001420.pool.root.1',
]

o = ROOT.TFile("beamStudy.root","RECREATE")
o.cd()
bHist = ROOT.TH2D("b","b", 400, -20, 20, 20, 30, 50)
cHist = ROOT.TH2D("c","c", 400, -20, 20, 20, 30, 50)
lHist = ROOT.TH2D("l","l", 400, -20, 20, 20, 30, 50)

def find_jets_btagged(fileName):

    f = ROOT.TFile.Open(fileName)
    # This step is important
    t = ROOT.xAOD.MakeTransientTree(f, 'CollectionTree')
    N = t.GetEntries()
    for i in range(N):
        t.GetEntry(i)
        # Get the PFlow jet container
        event = t.EventInfo
        # This method can be used to access auxdata in PyRoot: auxdataConst[type] (variable), it takes strings as arguments  
        beamZ = event.auxdataConst["float"] ("beamPosSigmaZ")
        jets = t.AntiKt4EMPFlowJets
        # Loop over PFlow jets
        for j in range(len(jets)):
          jet = jets.at(j)
          if jet.pt() < 250000 or abs(jet.eta()) > 2.5:
            continue
          flavor = jet.auxdataConst["int"] ("HadronConeExclTruthLabelID")
          # Load the ROOT macro to get b-tagging scores
          btagscore = ROOT.getBtaggingScore(jet, "DL1dv01")
          if flavor == 0:
            lHist.Fill(btagscore, beamZ)
          if flavor == 4:
            cHist.Fill(btagscore, beamZ)
          if flavor == 5:
            bHist.Fill(btagscore, beamZ)

for entry in files:
    find_jets_btagged(entry)
o.cd()
bHist.Write()
cHist.Write()
lHist.Write()
o.Close()
