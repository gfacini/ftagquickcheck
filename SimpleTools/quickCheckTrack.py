#!/usr/bin/env python
# Authored by Gabriel Facini <gabriel.facini@cern.ch>
# Useful to check tracking variables out of AOD directly to confirm behaviors seen in FTAG

from os import listdir
from os.path import isfile, join

import ctypes
import ROOT

# Initialize the xAOD infrastructure:
if(not ROOT.xAOD.Init().isSuccess()): print("Failed xAOD.Init()")

verbose = False

# Set up the input files:

#sample="mc21_13p6TeV.801661.Py8EG_A14NNPDF23LO_Zprime_tt_flatpT.merge.AOD.e8520_e8455_s3873_s3874_r14372_r14373"
#rucio list-file-replicas ${sample} | grep CERN | awk '{print $12}'
fileName = '/eos/atlas/atlasdatadisk/rucio/mc21_13p6TeV/19/03/AOD.32834168._000407.pool.root.1'

maxEvents = 5

#########################

treeName = "CollectionTree" # default when making transient tree anyway

totEvts = 0
totTrk  = 0
totTrkPseudo = 0


print("File: " + fileName)
f = ROOT.TFile.Open(fileName)

# Make the "transient tree":
t = ROOT.xAOD.MakeTransientTree( f, treeName)

if maxEvents < 0 or maxEvents > t.GetEntries():
    maxEvents = t.GetEntries()

# Print some information:
if verbose or True:
    print( "Number of input events: %s" % t.GetEntries() )
    print( "Considering N events:   %s" % maxEvents )

# make histos
h_nTrk = ROOT.TH1F("nTrk","nTrk",100,0,1000)
h_nBLayer = ROOT.TH1F("nBLayerHits","nBLayerHits",5,0,5)

for entry in range( 0, maxEvents ):
    t.GetEntry( entry )
    totEvts += 1
    if verbose:
        print( "Processing run #%i, event #%i" % ( t.EventInfo.runNumber(), t.EventInfo.eventNumber() ) )
        print( "Beam pos x, y, z: %i, %i, %i" % ( t.EventInfo.beamPosX(), t.EventInfo.beamPosY(), t.EventInfo.beamPosZ() ) )
        print( "Beam pos Sig x, y, z: %i, %i, %i" % ( t.EventInfo.beamPosSigmaX(), t.EventInfo.beamPosSigmaY(), t.EventInfo.beamPosSigmaZ() ) )

    nTrk       = len( t.InDetTrackParticles )
    h_nTrk.Fill( nTrk )

    iSummaryValue = ctypes.c_ubyte(-1)  # or ctypes.c_float(-1) for float type

    for track in t.InDetTrackParticles:
        track.summaryValue(iSummaryValue, ROOT.xAOD.expectNextToInnermostPixelLayerHit)
        print(iSummaryValue.value)
        h_nBLayer.Fill(iSummaryValue.value)

    #nTrkPseudo = len( t.InDetPseudoTrackParticles )
    if verbose:
        print( "Number of tracks        : %i" % nTrk )
    #    print( "Number of Pseudo tracks : %i" % nTrkPseudo )
    totTrk    += nTrk

    #totTrkPseudo += nTrkPseudo
    #if nTrk > 0: 
    #    print("Found a track!")
    #if nTrkPseudo > 0: 
    #    print("Found a Pseudo track!")



    pass # end loop over entries

# clear transient trees to avoid crash at end of job 
print("total events     : %i" % totEvts )
print("total tracks     : %i" % totTrk )
#print("total Pseudo trks: %i" % totTrkPseudo )

of = ROOT.TFile.Open("output_nBLayerTest.root","RECREATE")
of.cd()
h_nTrk.Write()
h_nBLayer.Write()
of.Close()

ROOT.xAOD.ClearTransientTrees()

